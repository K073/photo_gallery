const mongoose = require('mongoose');
const config = require('./config');

const Post = require('./src/models/photo');
const User = require('./src/models/user');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('photos');
    await db.dropCollection('users');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [first, second] = await User.create({
    username: 'user',
    facebookId: '123124214'
  }, {
    username: 'user',
    facebookId: '123124214'
  });

  await Post.create({
    title: 'Intel Core i7',
    image: 'cpu.jpg',
    user: first._id,
  }, {
    title: 'Seagate 3TB',
    image: 'hdd.jpg',
    user: second._id
  });

  db.close();
});