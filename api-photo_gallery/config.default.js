const path = require('path');
const rootPath = __dirname;

module.exports = {
  db: {
    url: 'mongodb://localhost:27017',
    name: 'gallery'
  },
  facebook: {
    appId: "000",
    appSecret: "000"
  },
  jwt: {
    secret: 'secret',
    expires: '64000'
  }
};
