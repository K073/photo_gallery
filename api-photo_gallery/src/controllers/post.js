const express = require('express');

const auth = require('../middleware/auth');
const Post = require('../models/photo');

const upload = require('../middleware/upload');

const createRouter = () => {
  const router = express.Router();

  router.post('/', [auth, upload.single('image')],  (req, res) => {
    const postData = req.body;
    postData.user = req.userId;

    if (req.file) {
      postData.image = req.file.filename;
    } else {
      postData.image = null;
    }

    const news = new Post(postData);

    news.save()
      .then(news => res.send(news))
      .catch(err => res.status(200).send(err))
  });

  router.get('/', async (req, res) => {
    try {
      const post = await Post.find().populate('user');

      res.send({data: post});
    } catch (err) {
      res.status(400).send(err);
    }
  });

  router.get('/user/:id', async (req, res) => {
    try {
      const post = await Post.find({user: req.params.id}).populate('user');

      if (!post) return res.status(404).send({err: 'posts not found'});

      res.send({data: post})
    } catch (e) {
      res.status(400).send({err: e})
    }
  });

  router.delete('/:id', auth,async (req, res) => {
    try {
      const post = await Post.findById({_id: req.params.id}).populate('user');
      if (!post) return res.status(404).send({err: 'posts not found'});

      if (!post.user._id.equals(req.userId)) return res.status(400).send({err: 'Ti kto taki? pnh'});

      post.remove();

      res.send(post);
    } catch (err) {
      res.status(400).send({err: err})
    }
  });

  return router
};

module.exports = createRouter;