const express = require('express');
const request = require('request-promise-native');
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const config = require('../../config');

const createRouter = () => {
  const router = express.Router();

  router.post('/facebookLogin', async (req, res) => {
    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${req.body.accessToken}&access_token=${config.facebook.appId}|${config.facebook.appSecret}`

    try {
      const response = await request(debugTokenUrl);

      const decodedResponse = JSON.parse(response);

      if (decodedResponse.data.error) {
        return res.status(401).send({message: 'Facebook token incorrect'});
      }

      if (req.body.id !== decodedResponse.data.user_id) {
        return res.status(401).send({message: 'Wrong user ID'});
      }

      let user = await User.findOne({facebookId: req.body.id});

      if (!user) {
        user = new User({
          facebookId: req.body.userID,
          username: req.body.name,
          email: req.body.email
        });

        await user.save();
      }

      const token = jwt.sign({id: user._id}, config.jwt.secret, {
        expiresIn: config.jwt.expires
      });

      return res.send({message: 'Login or register successful', user, token});

    } catch (error) {
      return res.status(401).send({message: 'Facebook token incorrect'});
    }
  });

  return router;
};

module.exports = createRouter;