const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const photoSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users',
    required: true
  },
  title: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  }
});

module.exports =  mongoose.model('posts', photoSchema);