const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = Schema({
  facebookId: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  }
});


module.exports = mongoose.model('users', UserSchema);