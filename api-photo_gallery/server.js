const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');

const Post = require('./src/controllers/post');
const User = require('./src/controllers/user');

const app = express();

const port = process.env.PORT || 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', () => {
  console.log('Mongoose connected!');

  app.use('/post', Post());
  app.use('/user', User());

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});
