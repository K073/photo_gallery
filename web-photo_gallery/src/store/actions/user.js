import axios from '../../axios';
import {push} from "react-router-redux";
import {LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS, USER_LOGOUT} from "./actionTypes";

const loginUserSuccess = (user, token) => {
  return {type: LOGIN_USER_SUCCESS, user, token};
};

const loginUserFailure = error => {
  return {type: LOGIN_USER_FAILURE, error};
};

export const facebookLogin = data => {
  return dispatch => {
    axios.post('/user/facebookLogin', data).then(
      response => {
        dispatch(loginUserSuccess(response.data.user, response.data.token));
        dispatch(push('/'));
      },
      error => {
        dispatch(loginUserFailure(error.response.data));
      }
    )
  };
};

export const userLogout = () => {
  return {type: USER_LOGOUT}
};