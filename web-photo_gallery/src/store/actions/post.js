import axios from '../../axios';
import {push} from "react-router-redux";
import {DELETE_POSTS_SUCCESS, FETCH_POSTS_SUCCESS, SEND_POST_SUCCESS} from "./actionTypes";

const sendPostSuccess = () => {
  return {type: SEND_POST_SUCCESS}
};

export const sendPost = (postData) => {
  return dispatch => {
    axios.post('/post', postData).then(
      response => {
        dispatch(sendPostSuccess());
        dispatch(push('/'));
      }
    )
  }
};

const fetchPostsSuccess = (data) => {
  return {type: FETCH_POSTS_SUCCESS, data}
};

export const fetchPosts = () => {
  return dispatch => {
    axios.get('/post').then(
      response => {
        dispatch(fetchPostsSuccess(response.data))
      }
    )
  }
};

export const fetchUserPosts = (id) => {
  return dispatch => {
    axios.get('/post/user/' + id).then(
      response => {
        dispatch(fetchPostsSuccess(response.data))
      }
    )
  }
};

const deleteUserPostsSuccess = (data) => {
  return {type: DELETE_POSTS_SUCCESS, data}
};

export const deleteUserPosts = (id) => {
  return dispatch => {
    axios.delete('/post/' + id).then(
      response => {
        dispatch(deleteUserPostsSuccess(response.data))
        dispatch(push('/'));
        dispatch(fetchPosts());
      }
    )
  }
};