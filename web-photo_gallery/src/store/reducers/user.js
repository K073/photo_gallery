import {LOGIN_USER_SUCCESS, USER_LOGOUT} from "../actions/actionTypes";

const initialState = {
  user: {
    username: '',
    facebookId: '',
  },
  token: '',
  auth: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_USER_SUCCESS:
      return {...state, user: action.user, token: action.token, auth: true};
    case USER_LOGOUT:
      return {...state, user: {}, token: '', auth: false};
    default:
      return state;
  }
};

export default reducer;