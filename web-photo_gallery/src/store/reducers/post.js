import {FETCH_POSTS_SUCCESS} from "../actions/actionTypes";

const initialState = {
  postList: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POSTS_SUCCESS:
      return {...state, postList: action.data.data};
    default:
      return state;
  }
};

export default reducer;