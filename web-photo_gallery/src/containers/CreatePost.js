import React from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, Grid, PageHeader, Row} from "react-bootstrap";
import FormElement from "../UI/FormElement";
import {sendPost} from "../store/actions/post";

class Layout extends React.Component {
  state = {
    title: '',
    image: ''
  };

  send = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.createPost(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  render() {
    return (
      <Grid>
        <Row>
          <Col>
            <PageHeader>
              Create post
            </PageHeader>
            <Form horizontal onSubmit={this.send}>
              <FormElement
                propertyName="title"
                title="Product title"
                type="text"
                value={this.state.title}
                changeHandler={this.inputChangeHandler}
                required
              />
              <FormElement
                propertyName="image"
                title="Product image"
                type="file"
                changeHandler={this.fileChangeHandler}
              />

              <FormGroup>
                <Col smOffset={2} sm={10}>
                  <Button bsStyle="primary" type="submit">Save</Button>
                </Col>
              </FormGroup>
            </Form>
          </Col>
        </Row>
      </Grid>
    )
  }
}

const initMapStateToProps = state => {
  return {
  }
};

const initMapDispatchToProps = dispatch => {
  return {
    createPost: postDate => dispatch(sendPost(postDate))
  }
};

export default connect(initMapStateToProps, initMapDispatchToProps)(Layout);