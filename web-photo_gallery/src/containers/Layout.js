import React from 'react';
import {connect} from "react-redux";
import {Navbar} from "react-bootstrap";
import {userLogout} from "../store/actions/user";
import Header from "../components/Header";

class Layout extends React.Component {
  render() {
    return (
      <div>
        <div>
          <Header user={this.props.user} auth={this.props.auth} logout={this.props.logout}/>
          {this.props.children}
        </div>
        <Navbar fluid>
          <Navbar.Text>
            © K073 norm paren'
          </Navbar.Text>
        </Navbar>
      </div>
    )
  }
}

const initMapStateToProps = state => {
  return {
    user : state.user.user,
    auth : state.user.auth
  }
};

const initMapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(userLogout())
  }
};

export default connect(initMapStateToProps, initMapDispatchToProps)(Layout);