import React from 'react';
import {connect} from "react-redux";
import {Button, Col, Grid, PageHeader, Row} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {deleteUserPosts, fetchPosts, fetchUserPosts} from "../store/actions/post";
import PostItem from "../components/PostItem";

class Layout extends React.Component {

  componentDidUpdate(prevProps) {
    if (this.props.match.params !== prevProps.match.params) {
      if (this.props.match.params.id) {
        this.props.fetchUsersPosts(this.props.match.params.id)
      } else {
        this.props.fetchPosts();
      }
    }
  }

  componentDidMount() {
    this.props.fetchPosts();
  }
  render() {
    console.log(this.props);
    return (
      <Grid>
        <Row>
          <Col>
            <PageHeader className={'clearfix'}>
              Gallery <span hidden={!this.props.auth}>
              <LinkContainer to={'/create-post'}><Button className={'pull-right'}>Send post</Button></LinkContainer>
            </span>
            </PageHeader>
            {this.props.posts && this.props.posts.map(value =>
              <Col md={3} key={value._id}><PostItem
                username={value.user.username}
                image={value.image}
                title={value.title}
                id={value._id}
                userId={value.user._id}
                isAdmin={this.props.userId === value.user._id}
                delete={this.props.deletePost}
              /></Col>)}
          </Col>
        </Row>
      </Grid>
    )
  }
}

const initMapStateToProps = state => {
  return {
    auth : state.user.auth,
    userId: state.user.user._id,
    posts : state.post.postList
  }
};

const initMapDispatchToProps = dispatch => {
  return {
    fetchPosts: () => dispatch(fetchPosts()),
    fetchUsersPosts: (userId) => dispatch(fetchUserPosts(userId)),
    deletePost: (postId) => dispatch(deleteUserPosts(postId))
  }
};

export default connect(initMapStateToProps, initMapDispatchToProps)(Layout);