import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import './index.css';
import axios from './axios';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import store, {history} from './store/configureStore';
import {ConnectedRouter} from "react-router-redux";

axios.interceptors.request.use(config => {
  try {
    config.headers['x-access-token'] = store.getState().user.token;
  } catch (e) {
    // do nothing
  }

  return config;
});


const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();