import React from 'react';
import {Button, Image, Modal, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../config';

import notFound from '../assets/image-not-found.jpg';

class ProductListItem extends React.Component{
  state = {
    modal: false
  };

  toggleModal = () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }))
  };

  render() {
    let image = notFound;

    if (this.props.image) {
      image = config.apiUrl + '/uploads/' + this.props.image;
    }

    return (
      <Panel>
        <Panel.Body>
          {this.props.isAdmin && <Button className={'pull-right'} onClick={() => this.props.delete(this.props.id)}>X</Button>}
          <Image
            onClick={this.toggleModal}
            src={image}
            thumbnail
          />
          <div>
            {this.props.title}
          </div>
          <Link exact to={'/' + this.props.userId}>
            {this.props.username}
          </Link>
        </Panel.Body>
        <div className="static-modal">
          <Modal show={this.state.modal} onHide={this.toggleModal}>

            <Modal.Body>
              <Image
                src={image}
                thumbnail
              />
            </Modal.Body>

            <Modal.Footer>
              <Button onClick={this.toggleModal}>Close</Button>
            </Modal.Footer>
          </Modal>
        </div>
      </Panel>
    );
  }
};

ProductListItem.propTypes = {
  title: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired
};

export default ProductListItem;