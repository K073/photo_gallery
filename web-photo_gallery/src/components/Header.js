import React from 'react';
import {Button, MenuItem, Modal, Nav, Navbar, NavDropdown, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import FacebookLogin from "./FacebookLogin";

class Header extends React.Component {
  state = {
    modal: false
  };

  toggleModal = () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }))
  };

  render() {
    return (
      <Navbar inverse collapseOnSelect staticTop>
        <Navbar.Header>
          <Navbar.Brand>
            <LinkContainer to={'/'}>
              <a>Photo Gallery</a>
            </LinkContainer>
          </Navbar.Brand>
          <Navbar.Toggle/>
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav pullRight>
            <LinkContainer exact to={'/'}>
              <NavItem>
                Gallery
              </NavItem>
            </LinkContainer>
            {!this.props.auth ?
              <NavItem onClick={this.toggleModal}>
                Login
              </NavItem> :
              <NavDropdown title={this.props.user.username} id="basic-nav-dropdown">
                <MenuItem onClick={() => this.props.logout()}>Logout</MenuItem>
              </NavDropdown>
            }
          </Nav>
        </Navbar.Collapse>
        <div className="static-modal">
          <Modal show={this.state.modal} onHide={this.toggleModal}>
            <Modal.Header>
              <Modal.Title>Login</Modal.Title>
            </Modal.Header>

            <Modal.Body>
              <FacebookLogin hideLogin={this.toggleModal}/>
            </Modal.Body>

            <Modal.Footer>
              <Button onClick={this.toggleModal}>Close</Button>
            </Modal.Footer>
          </Modal>
        </div>
      </Navbar>
    )
  }
}

export default Header;