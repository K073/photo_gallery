import React from 'react';
import './App.css';
import Layout from "./containers/Layout";
import {Route, Switch} from "react-router-dom";
import Gallery from "./containers/Gallery";
import CreatePost from "./containers/CreatePost";

class App extends React.Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path={'/'} exact component={Gallery}/>
          <Route path={'/create-post'} exact component={CreatePost}/>
          <Route path={'/:id'} exact component={Gallery}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
